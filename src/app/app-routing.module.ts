import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ContactComponent } from './contact/contact.component';
import { Page404Component } from './page404/page404.component';
import { AboutComponent } from './about/about.component';
import { BrowserModule } from '@angular/platform-browser';
import { ItemComponent } from './item/item.component';
import { ItemDetalisComponent } from './item-detalis/item-detalis.component';
import { ItemStatComponent } from './item-stat/item-stat.component';


const itemRoutes: Routes = [
  { path: 'details', component: ItemDetalisComponent},
  { path: 'stat', component: ItemStatComponent},
];

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'product', component: ContactComponent },
  { path: 'about', component: AboutComponent },
  { path: 'item/:id', component: ItemComponent },
  { path: 'item/:id', component: ItemComponent, children: itemRoutes },
  { path: '**', component: Page404Component }
];

@NgModule({
  imports: [BrowserModule, RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
