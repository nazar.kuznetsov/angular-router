import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ContactComponent } from './contact/contact.component';
import { Page404Component } from './page404/page404.component';
import { AboutComponent } from './about/about.component';
import { RouterModule } from '@angular/router';
import { ItemComponent } from './item/item.component';
import { ItemDetalisComponent } from './item-detalis/item-detalis.component';
import { ItemStatComponent } from './item-stat/item-stat.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ContactComponent,
    Page404Component,
    AboutComponent,
    ItemComponent,
    ItemDetalisComponent,
    ItemStatComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
