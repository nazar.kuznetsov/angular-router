import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent {

  private id: number;
  private routeSubscription: Subscription;

  constructor(private route: ActivatedRoute) {

    this.routeSubscription = route.params.subscribe(params => this.id = params['id']);
  }
}
